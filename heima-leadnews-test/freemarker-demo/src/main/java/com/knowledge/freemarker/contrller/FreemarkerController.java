package com.knowledge.freemarker.contrller;

import com.knowledge.freemarker.entity.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class FreemarkerController {
    @GetMapping("/hello")
    public String hello(Model model){
        model.addAttribute("name","yzzzzzz");
        Student student = new Student();
        student.setAge(18);
        student.setName("yuanzhe");
        model.addAttribute("stu",student);
        return "01-basic";
    }
}
