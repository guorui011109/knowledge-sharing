package com.knowledge.freemarker.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Student {
    private String name;
    private int age;
    private LocalDateTime birthday;
    private Float money;
}
