package com.tess4J;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.io.File;

public class Application {
    public static void main(String[] args) throws TesseractException {
        ITesseract tesseract = new Tesseract();
        tesseract.setDatapath("D:\\guorui\\tessData");
        tesseract.setLanguage("chi_sim");
        File file = new File("d:\\Users\\YQSL\\Pictures\\143.jpeg");
        String result = tesseract.doOCR(file);
        System.out.println(result);
    }
}
