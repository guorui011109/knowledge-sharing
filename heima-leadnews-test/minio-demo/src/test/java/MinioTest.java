import com.heima.file.service.FileStorageService;
import com.knowledge.minio.MinioApplication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest(classes = MinioApplication.class)
@RunWith(SpringRunner.class)
public class MinioTest {
    @Autowired
    private FileStorageService fileStorageService;
    @Test
    public void testMinIO() throws FileNotFoundException {
        String path = fileStorageService.uploadHtmlFile("", "hkd.html", new FileInputStream("D://hkd.html"));
        System.out.println(path);
    }
    /* public static void main(String[] args) throws Exception {
        MinioClient minioClient = MinioClient.builder().credentials("minioadmin", "minioadmin").endpoint("http://47.113.197.91:9000").build();
        FileInputStream fileInputStream = new FileInputStream("D://hkd.html");
        PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                .object("hkd.html")
                .contentType("text/html")
                .bucket("knowledge-share")
                .stream(fileInputStream,fileInputStream.available(),-1).build();
        minioClient.putObject(putObjectArgs);
        System.out.println("http://47.113.197.91:9000/knowledge-share/hkd.html");
        fileInputStream.close();
    } */
}