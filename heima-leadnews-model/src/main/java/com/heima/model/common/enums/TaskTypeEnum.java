package com.heima.model.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TaskTypeEnum {
    NEWS_SCAN_TIME(1001,1,"文章定时审核"),
    REMOTE_ERROR(1002,2,"第三方接口调用失败重试");
    private final int type;
    private final int priority;
    private final String desc;
}
