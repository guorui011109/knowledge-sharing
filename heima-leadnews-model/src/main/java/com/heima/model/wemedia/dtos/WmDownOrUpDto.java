package com.heima.model.wemedia.dtos;

import lombok.Data;

@Data
public class WmDownOrUpDto {
    private String id;
    private int enable;
}
