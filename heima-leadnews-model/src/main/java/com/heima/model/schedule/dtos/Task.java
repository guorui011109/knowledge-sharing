package com.heima.model.schedule.dtos;

import lombok.Data;

@Data
public class Task {
    /**
     * 任务id
     */
    private Long taskId;
    private Integer taskType;
    /**
     * 优先级
     */
    private Integer priority;
    /**
     * 执行id
     */
    private Long executeTime;
    /**
     * 执行参数
     */
    private byte[] parameters;
}
