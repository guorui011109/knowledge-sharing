package com.heima.wemedia.controller.v1;

import com.heima.model.admin.dtos.AdSensitiveListDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmSensitive;
import com.heima.wemedia.service.WmSensitiveService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/v1/sensitive")
public class WmSensitiveController {
    @Resource
    private WmSensitiveService wmSensitiveService;
    /**
     * 敏感词列表
     */
    @PostMapping("/list")
    public ResponseResult list(@RequestBody AdSensitiveListDto dto){
        return wmSensitiveService.list(dto);
    }
    @PostMapping("/save")
    public ResponseResult insert(@RequestBody WmSensitive wmSensitive){
        return wmSensitiveService.insert(wmSensitive);
    }

    @PostMapping("/update")
    public ResponseResult update(@RequestBody WmSensitive wmSensitive){
        return wmSensitiveService.update(wmSensitive);
    }

    @DeleteMapping("/del/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id){
        return wmSensitiveService.delete(id);
    }
}
