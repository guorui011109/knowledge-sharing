package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.constant.WemediaConstants;
import com.heima.common.exception.CustomException;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmDownOrUpDto;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.model.wemedia.vo.WmNewsVo;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmNewsAutoScanService;
import com.heima.wemedia.service.WmNewsService;
import com.heima.wemedia.service.WmNewsTaskService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {
    @Resource
    private WmNewsMapper wmNewsMapper;
    @Resource
    private WmNewsMaterialMapper wmNewsMaterialMapper;
    @Resource
    private WmMaterialMapper wmMaterialMapper;
    @Resource
    private WmNewsAutoScanService wmNewsAutoScanService;
    @Resource
    private WmNewsTaskService wmNewsTaskService;
    @Override
    public ResponseResult find(WmNewsPageReqDto dto) {
        //1.检查参数
        dto.checkParam();
        //2.分页条件查询
        IPage<WmNews> page = new Page<>(dto.getPage(), dto.getSize());
        QueryWrapper<WmNews> queryWrapper = new QueryWrapper<>();
        if (dto.getStatus() != null) {
            queryWrapper.eq("status", dto.getStatus());
        }

        if (StringUtils.isNotBlank(dto.getKeyword())) {
            queryWrapper.like("title", dto.getKeyword());
        }
        if (dto.getChannelId() != null){
            queryWrapper.eq("channel_id",dto.getChannelId());
        }
        if (dto.getBeginPubDate() != null && dto.getEndPubDate() != null){
            queryWrapper.between("publish_time",dto.getBeginPubDate(),dto.getEndPubDate());
        }
        queryWrapper.eq("user_id", WmThreadLocalUtil.getUser().getId());
        queryWrapper.orderByDesc("publish_time");
        page = wmNewsMapper.selectPage(page, queryWrapper);
        //3.结果返回
        ResponseResult responseResult = new PageResponseResult(dto.getPage(),dto.getSize(),(int)page.getTotal());
        responseResult.setData(page.getRecords());
        return responseResult;
    }

    @Override
    public ResponseResult submit(WmNewsDto dto) {
        //条件判断
        if (dto == null || dto.getContent() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //1.保存或修改文章
        WmNews wmNews = new WmNews();
        BeanUtils.copyProperties(dto,wmNews);
        if (dto.getImages() != null && dto.getImages().size() > 0){
            String images = StringUtils.join(dto.getImages(), ",");
            wmNews.setImages(images);
        }
            //若当前封面类型为自动
        if (dto.getType().equals(WemediaConstants.WM_NEWS_TYPE_AUTO)){
            wmNews.setType(null);
        }
        saveOrUpdateNews(wmNews);
        //2.判断是否为草稿，若是则结束
        if (dto.getStatus().equals(WmNews.Status.NORMAL.getCode())){
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }
        //3.若不是草稿，则保存文章内容与素材的关系
        List<String> materials = extractUrlInfo(dto.getContent());
        saveRelativeInfoContent(materials,dto.getId());
        //4.保存文章封面图片与素材的关系,如果布局是自动，则需要匹配封面图片
        saveRelativeInfoCover(dto,wmNews,materials);
        try {
            wmNewsTaskService.addNewsToTask(wmNews.getId(),wmNews.getPublishTime());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult deleteNews(int id) {
        wmNewsMapper.deleteById(id);
        return ResponseResult.okResult(200,"删除成功");
    }

    @Override
    public ResponseResult downOrUp(WmDownOrUpDto dto) {
        WmNews wmNews = wmNewsMapper.selectById(dto.getId());
        if (wmNews == null){
            throw new CustomException(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        wmNews.setEnable((short)dto.getEnable());
        wmNewsMapper.updateById(wmNews);
        if (dto.getEnable() == 1) {
            return ResponseResult.okResult(200,"上架成功" );
        }
        if (dto.getEnable() == 0) {
            return ResponseResult.okResult(200, "下架成功");
        }
        return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
    }

    // 封面匹配规则
           /* 1.如果内容图片大于等于1，小于3 单图 type 1
           2.如果内容图片大于等于3，多图 type 3
           3.如果内容无图片 无图 type 0
           4.保存封面与图片素材关系
         */
    private void saveRelativeInfoCover(WmNewsDto dto,WmNews wmNews,List<String> materials){
        //自动匹配规则
        List<String> images = dto.getImages();
        if (dto.getType().equals(WemediaConstants.WM_NEWS_TYPE_AUTO)){
              //多图
              if (materials.size() >= 3){
                  wmNews.setType(WemediaConstants.WM_NEWS_MANY_IMAGE);
                  images = materials.stream().limit(3).collect(Collectors.toList());
              } else if (materials.size() >= 1) {
                  //单图
                  wmNews.setType(WemediaConstants.WM_NEWS_SINGLE_IMAGE);
                  images = materials.stream().limit(1).collect(Collectors.toList());
              } else {
                  //无图
                  wmNews.setType(WemediaConstants.WM_NEWS_NONE_IMAGE);
              }
              //修改文章
            if (images != null && images.size() > 0){
                wmNews.setImages(StringUtils.join(images,","));
            }
            updateById(wmNews);
          }
        //保存封面图片与素材的关系
        if (images != null && images.size() > 0) {
            saveRelativeInfo(images, wmNews.getId(), WemediaConstants.WM_COVER_REFERENCE);
        }
    }
    private void saveOrUpdateNews(WmNews wmNews){
        wmNews.setUserId(WmThreadLocalUtil.getUser().getId());
        wmNews.setCreatedTime(new Date());
        wmNews.setSubmitedTime(new Date());
        wmNews.setEnable((short)1);//默认上架
        //判断保存还是修改
        if (wmNews.getId() == null){
            //保存
            wmNewsMapper.insert(wmNews);
        } else {
            //修改
            //删除图片与素材的关系
            wmNewsMaterialMapper.deleteById(wmNews.getId());
            wmNewsMapper.updateById(wmNews);
        }
    }
    //保存文章内容与素材的关系
    private void saveRelativeInfoContent(List<String> materials,Integer newsId){
        saveRelativeInfo(materials,newsId,WemediaConstants.WM_CONTENT_REFERENCE);
    }
    //保存文章图片与素材的关系于数据库中
    private void saveRelativeInfo(List<String> materials, Integer newsId, Short type) {
        if (materials != null && !materials.isEmpty()){
            List<WmMaterial> dbMaterials = wmMaterialMapper.selectList(new QueryWrapper<WmMaterial>()
                    .in("url", materials));
            //判断素材是否生效
            if (dbMaterials == null || dbMaterials.size() == 0){
                throw new CustomException(AppHttpCodeEnum.MATERIAL_REFERENCE_FAIL);
            }
            if (materials.size() != dbMaterials.size()){
                throw new CustomException(AppHttpCodeEnum.MATERIAL_REFERENCE_FAIL);
            }
            List<Integer> idList = dbMaterials.stream().map(WmMaterial::getId).collect(Collectors.toList());
            wmNewsMaterialMapper.saveRelations(idList,newsId,type);
        }
    }

    //获取content中的图片信息
    private List<String> extractUrlInfo(String content){
        List<String> imageUrls = new ArrayList<>();
        List<Map> maps = JSON.parseArray(content, Map.class);
        for (Map map : maps) {
            if (map.get("type").equals("image")){
                String imageUrl = (String) map.get("value");
                imageUrls.add(imageUrl);
            }
        }
        return imageUrls;
    }
    @Override
    public ResponseResult findList(NewsAuthDto dto) {
        //1.参数检查
        dto.checkParam();

        //记录当前页
        int currentPage = dto.getPage();

        //2.分页查询+count查询
        dto.setPage((dto.getPage()-1)*dto.getSize());
        List<WmNewsVo> wmNewsVoList = wmNewsMapper.findListAndPage(dto);
        int count = wmNewsMapper.findListCount(dto);

        //3.结果返回
        ResponseResult responseResult = new PageResponseResult(currentPage,dto.getSize(),count);
        responseResult.setData(wmNewsVoList);
        return responseResult;
    }

    @Resource
    private WmUserMapper wmUserMapper;

    /**
     * 查询文章详情
     * @param id
     * @return
     */
    @Override
    public ResponseResult findWmNewsVo(Integer id) {
        //1.检查参数
        if(id == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.查询文章信息
        WmNews wmNews = getById(id);
        if(wmNews == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //3.查询用户信息
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());

        //4.封装vo返回
        WmNewsVo vo = new WmNewsVo();
        //属性拷贝
        BeanUtils.copyProperties(wmNews,vo);
        if(wmUser != null){
            vo.setAuthorName(wmUser.getName());
        }

        ResponseResult responseResult = new ResponseResult().ok(vo);

        return responseResult;
    }

    /**
     * 文章审核，修改状态
     * @param status 2  审核失败  4 审核成功
     * @param dto
     * @return
     */
    @Override
    public ResponseResult updateStatus(Short status, NewsAuthDto dto) {
        //1.检查参数
        if(dto == null || dto.getId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.查询文章信息
        WmNews wmNews = getById(dto.getId());
        if(wmNews == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //3.修改文章的状态
        wmNews.setStatus(status);
        if(StringUtils.isNotBlank(dto.getMsg())){
            wmNews.setReason(dto.getMsg());
        }
        updateById(wmNews);

        //审核成功，则需要创建app端文章数据，并修改自媒体文章
        if(status.equals(WemediaConstants.WM_NEWS_AUTH_PASS)){
            //创建app端文章数据
            ResponseResult responseResult = wmNewsAutoScanService.saveAppArticle(wmNews);
            if(responseResult.getCode().equals(200)){
                wmNews.setArticleId((Long) responseResult.getData());
                wmNews.setStatus(WmNews.Status.PUBLISHED.getCode());
                updateById(wmNews);
            }
        }

        //4.返回
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


}
