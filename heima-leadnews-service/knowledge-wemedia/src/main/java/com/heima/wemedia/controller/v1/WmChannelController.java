package com.heima.wemedia.controller.v1;

import com.heima.model.admin.dtos.AdSensitiveListDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/channel")
public class WmChannelController {
    @Resource
    private WmChannelService wmChannelService;

    /**
     * 查找所有频道
     * @return
     */
    @GetMapping("/channels")
    public ResponseResult findAll(){
        return wmChannelService.findAllChannels();
    }
    @PostMapping("/list")
    public ResponseResult list(@RequestBody AdSensitiveListDto dto){
        return wmChannelService.listChannels(dto);
    }
    @PostMapping("/save")
    public ResponseResult insert(@RequestBody WmChannel adChannel){
        return wmChannelService.insert(adChannel);
    }

    @PostMapping("/update")
    public ResponseResult update(@RequestBody WmChannel adChannel){
        return wmChannelService.update(adChannel);
    }

    @GetMapping("/del/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id){
        return wmChannelService.delete(id);
    }
}
