package com.heima.wemedia.controller.v1;

import com.heima.common.constant.WemediaConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmDownOrUpDto;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
//TODO 1.下架文章，2.编辑内容
@RequestMapping("/api/v1/news")
@RestController
public class WmNewsController {
    @Resource
    private WmNewsService wmNewsService;
    /**
     * 条件查询文章列表
     * @param dto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult find(@RequestBody WmNewsPageReqDto dto){
        return wmNewsService.find(dto);
    }

    /**
     * 发布修改文章或保存为草稿
     * @param wmNewsDto
     * @return
     */
    @PostMapping("/submit")
    public ResponseResult submit(@RequestBody WmNewsDto wmNewsDto) throws Exception {
        return wmNewsService.submit(wmNewsDto);
    }

    @GetMapping("del_news/{id}")
    public ResponseResult deleteNews(@PathVariable int id){
        return wmNewsService.deleteNews(id);
    }

    @PostMapping("down_or_up")
    public ResponseResult downOrUp(@RequestBody WmDownOrUpDto dto){
        return wmNewsService.downOrUp(dto);
    }
    @PostMapping("/list_vo")
    public ResponseResult findList(@RequestBody NewsAuthDto dto){
        return wmNewsService.findList(dto);
    }

    @GetMapping("/one_vo/{id}")
    public ResponseResult findWmNewsVo(@PathVariable("id") Integer id){
        return wmNewsService.findWmNewsVo(id);
    }

    @PostMapping("/auth_pass")
    public ResponseResult authPass(@RequestBody NewsAuthDto dto){
        return wmNewsService.updateStatus(WemediaConstants.WM_NEWS_AUTH_PASS,dto);
    }

    @PostMapping("/auth_fail")
    public ResponseResult authFail(@RequestBody NewsAuthDto dto){
        return wmNewsService.updateStatus(WemediaConstants.WM_NEWS_AUTH_FAIL,dto);
    }
}
