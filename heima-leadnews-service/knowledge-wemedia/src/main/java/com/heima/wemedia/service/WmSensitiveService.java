package com.heima.wemedia.service;

import com.heima.model.admin.dtos.AdSensitiveListDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmSensitive;

public interface WmSensitiveService {
    ResponseResult list(AdSensitiveListDto dto);
    ResponseResult insert(WmSensitive wmSensitive);
    ResponseResult update(WmSensitive wmSensitive);
    ResponseResult delete(Integer id);
}
