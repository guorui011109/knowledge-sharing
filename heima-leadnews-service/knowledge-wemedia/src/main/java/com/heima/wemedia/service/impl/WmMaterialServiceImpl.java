package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Service
@Slf4j
@Transactional
public class WmMaterialServiceImpl  extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService{
    @Resource
    private FileStorageService fileStorageService;
    @Resource
    private WmMaterialMapper wmMaterialMapper;
    @Override
    public ResponseResult uploadPicture(MultipartFile multipartFile){
        //1.检查参数
        if (multipartFile == null || multipartFile.getSize() == 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.上传图片到minIO
        String fileName = UUID.randomUUID().toString().replace("-", "");
        String originalFilename = multipartFile.getOriginalFilename();
        String postfix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String path = null;
        try {
            path = fileStorageService.uploadImgFile("", fileName + postfix, multipartFile.getInputStream());
        } catch (IOException e){
            e.printStackTrace();
            log.error("上传图片到minIO失败");
        }
        //3.图片保存到数据库
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setUrl(path);
        wmMaterial.setUserId(WmThreadLocalUtil.getUser().getId());
        wmMaterial.setType((short)0);
        wmMaterial.setIsCollection((short)0);
        wmMaterial.setCreatedTime(new Date());
        wmMaterialMapper.insert(wmMaterial);
        //4.返回结果
        return ResponseResult.okResult(wmMaterial);
    }

    @Override
    public ResponseResult findList(WmMaterialDto wmMaterialDto) {
        //1.检查参数
        wmMaterialDto.checkParam();
        //2.分页查询
        IPage<WmMaterial> page = new Page<>(wmMaterialDto.getPage(), wmMaterialDto.getSize());
            //是否收藏
        if (wmMaterialDto.getIsCollection() != null && wmMaterialDto.getIsCollection() == 1) {
            page = wmMaterialMapper.selectPage(page, new QueryWrapper<WmMaterial>()
                    .eq("user_id", WmThreadLocalUtil.getUser().getId())
                    .eq("is_collection", 1)
                    .orderByDesc("created_time"));
        } else {
            page = wmMaterialMapper.selectPage(page, new QueryWrapper<WmMaterial>()
                    .eq("user_id", WmThreadLocalUtil.getUser().getId())
                    .orderByDesc("created_time"));
        }
        //3.结果返回
        ResponseResult responseResult = new PageResponseResult(wmMaterialDto.getPage(),wmMaterialDto.getSize(),(int)page.getTotal());
        responseResult.setData(page.getRecords());
        return responseResult;
    }

    @Override
    public ResponseResult deletePicture(Long id) {
        //1.参数校验
        if (id == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.根据id删除图片
        wmMaterialMapper.deleteById(id);
        //3.返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult collectPicture(Long id) {
        //1.参数校验
        if (id == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.根据id收藏图片
        WmMaterial wmMaterial = wmMaterialMapper.selectById(id);
        wmMaterial.setIsCollection((short)1);
        wmMaterialMapper.updateById(wmMaterial);
        //3.返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
    @Override
    public ResponseResult cancelCollectPicture(Long id) {
        //1.参数校验
        if (id == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.根据id收藏图片
        WmMaterial wmMaterial = wmMaterialMapper.selectById(id);
        wmMaterial.setIsCollection((short)0);
        wmMaterialMapper.updateById(wmMaterial);
        //3.返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
