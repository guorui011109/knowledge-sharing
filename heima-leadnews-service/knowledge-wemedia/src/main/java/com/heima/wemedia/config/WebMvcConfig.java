package com.heima.wemedia.config;

import com.heima.wemedia.interceptor.WmTokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    //增加自定义拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //使用了错误通配符"./**"导致拦截失败
       registry.addInterceptor(new WmTokenInterceptor()).addPathPatterns("/**");
    }
}
