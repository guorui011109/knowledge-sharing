package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
@RequestMapping("api/v1/material")
public class WmMaterialController {
    @Resource
    private WmMaterialService wmMaterialService;

    /**
     * 图片上传
     */
    @PostMapping("/upload_picture")
    public ResponseResult uploadPicture(MultipartFile multipartFile){
        return wmMaterialService.uploadPicture(multipartFile);
    }

    /**
     * 素材列表查询
     * @param wmMaterialDto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult findList(@RequestBody WmMaterialDto wmMaterialDto){
        return wmMaterialService.findList(wmMaterialDto);
    }

    /**
     * 删除图片
     * @param id
     * @return
     */
    @GetMapping("/del_picture/{id}")
    public ResponseResult deletePicture(@PathVariable Long id){
        return wmMaterialService.deletePicture(id);
    }

    /**
     * 收藏图片
     * @param id
     * @return
     */
    @GetMapping("/collect/{id}")
    public ResponseResult collectPicture(@PathVariable Long id) {
        return wmMaterialService.collectPicture(id);
    }

    /**
     * 取消收藏图片
     * @param id
     * @return
     */
    @GetMapping("/cancel_collect/{id}")
    public ResponseResult cancelCollectPicture(@PathVariable Long id) {
        return wmMaterialService.cancelCollectPicture(id);
    }
}
