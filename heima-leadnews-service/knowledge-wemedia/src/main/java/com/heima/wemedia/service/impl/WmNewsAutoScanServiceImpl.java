package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmSensitive;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.common.SensitiveWordUtil;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmSensitiveMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmNewsAutoScanService;
import com.knowledge.apis.article.IArticleClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class WmNewsAutoScanServiceImpl implements WmNewsAutoScanService {
    @Resource
    private WmNewsMapper wmNewsMapper;
    @Resource
    private IArticleClient articleClient;
    @Resource
    private WmChannelMapper wmChannelMapper;
    @Resource
    private WmUserMapper wmUserMapper;
    @Resource
    private WmSensitiveMapper wmSensitiveMapper;
    @Override
    public void autoScan(Integer id) {
        //1.根据id查询自媒体文章
        WmNews wmNews = wmNewsMapper.selectById(id);
        if (wmNews == null){
            throw new RuntimeException("WmNewsAutoScanServiceImpl-文章不存在");
        }
        if (wmNews.getStatus().equals(WmNews.Status.SUBMIT.getCode())){
            Map<String, Object> result = handleTextAndImage(wmNews);
            //自管理敏感词过滤
            boolean isSensitive = handleSensitiveScan(result.get("content").toString(), wmNews);
            if (!isSensitive) return;
            //2.阿里云接口审核文本
            boolean isTextScan = handleTextScan(result.get("content").toString(), wmNews);
            if (!isTextScan) return;
            //3.阿里云接口审核图片
            boolean isImageScan = handleImageScan((List<String>) result.get("images"), wmNews);
            if (!isImageScan) return;
        }
        //4.审核成功，保存app端文章数据
        ResponseResult<String> responseResult = saveAppArticle(wmNews);
        if (!responseResult.getCode().equals(200)){
            throw new RuntimeException("文章审核，保存app端相关文章失败");
        }
        wmNews.setArticleId(Long.parseLong(responseResult.getData()));
        wmNews.setStatus((short)9);
        wmNews.setReason("审核成功");
        wmNewsMapper.updateById(wmNews);
    }

    /**
     * 自管理敏感词过滤
     * @param content
     * @param wmNews
     * @return
     */
    private boolean handleSensitiveScan(String content, WmNews wmNews) {
        //获取所有敏感词
        List<WmSensitive> sensitive = wmSensitiveMapper.selectList(new QueryWrapper<WmSensitive>()
                .select("sensitives"));
        List<String> sensitives = sensitive.stream().map(WmSensitive::getSensitives).collect(Collectors.toList());
        //初始化敏感词库
        SensitiveWordUtil.initMap(sensitives);
        //查询文章中是否包含敏感词
        Map<String, Integer> result = SensitiveWordUtil.matchWords(content);
        if (result.size() > 0){
            wmNews.setStatus((short)2);
            wmNews.setReason("当前文章中存在违规内容" + result);
            wmNewsMapper.updateById(wmNews);
            return false;
        }
        return true;
    }

    public ResponseResult<String> saveAppArticle(WmNews wmNews) {
        ArticleDto articleDto = new ArticleDto();
        BeanUtils.copyProperties(wmNews,articleDto);
        articleDto.setLayout(wmNews.getType());
        WmChannel wmChannel = wmChannelMapper.selectById(wmNews.getChannelId());
        articleDto.setChannelName(wmChannel.getName());
        articleDto.setAuthorId(wmNews.getUserId().longValue());
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());
        articleDto.setAuthorName(wmUser.getName());
        articleDto.setId(wmNews.getArticleId());
        articleDto.setCreatedTime(new Date());
        return articleClient.saveArticle(articleDto);
    }


    //没开通阿里云，装模做样一下啊
    private boolean handleTextScan(String content, WmNews wmNews) {
        return true;
    }
    private boolean handleImageScan(List<String> images, WmNews wmNews) {
        return true;
    }
    /**
     * 从自媒体文章内容中提取文本和图片
     * 提取文章封面图片
     * @param wmnews
     * @return
     */
    private Map<String,Object> handleTextAndImage(WmNews wmnews){
        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<String> images = new ArrayList<>();
        if (StringUtils.isNotBlank(wmnews.getContent())){
            List<Map> maps = JSONArray.parseArray(wmnews.getContent(), Map.class);
            maps.forEach(map -> {
                if (map.get("type").equals("text")){
                    stringBuilder.append(map.get("value"));
                }
                if (map.get("type").equals("image")){
                    images.add((String) map.get("value"));
                }
            });
        }
        if (StringUtils.isNotBlank(wmnews.getImages())){
            String[] image = wmnews.getImages().split(",");
            images.addAll(Arrays.asList(image));
        }
        HashMap<String, Object> result = new HashMap<>();
        result.put("content",stringBuilder);
        result.put("images",images);
        return result;
    }
}
