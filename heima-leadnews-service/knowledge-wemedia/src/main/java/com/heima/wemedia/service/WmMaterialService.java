package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.multipart.MultipartFile;


public interface WmMaterialService extends IService<WmMaterial> {
    /**
     * 图片上传
     * @param multipartFile
     * @return
     */
    ResponseResult uploadPicture(MultipartFile multipartFile);

    /**
     * 素材列表查询
     * @param wmMaterialDto
     * @return
     */
    ResponseResult findList(WmMaterialDto wmMaterialDto);

    /**
     * 删除图片
     * @param id
     * @return
     */
    ResponseResult deletePicture(Long id);

    /**
     * 收藏图片
     * @param id
     * @return
     */
    ResponseResult collectPicture(Long id);

    /**
     * 取消收藏图片
     * @param id
     * @return
     */
    ResponseResult cancelCollectPicture(Long id);
}
