package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.AdSensitiveListDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;

public interface WmChannelService extends IService<WmChannel> {
    ResponseResult findAllChannels();
    ResponseResult listChannels(AdSensitiveListDto dto);
    ResponseResult insert(WmChannel wmChannel);
    ResponseResult update(WmChannel wmChannel);
    ResponseResult delete(Integer id);
}
