package com.leadnews.test;

import com.heima.common.aliyun.GreenTextScan;
import com.heima.wemedia.WemediaApplication;
import com.heima.wemedia.service.WmNewsAutoScanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Map;

@SpringBootTest(classes = WemediaApplication.class)
@RunWith(SpringRunner.class)
public class AliYunTest {
    @Resource
    private GreenTextScan greenTextScan;
    @Resource
    private WmNewsAutoScanService wmNewsAutoScanService;
    //文本审核测试
    @Test
    public void testScanText() throws Exception{
        Map map = greenTextScan.greenTextScan("我是一个好人,冰毒");
        System.out.println(map);
    }
    //图片审核测试
    @Test
    public void testScanImage() throws Exception {
        wmNewsAutoScanService.autoScan(6235);
    }
}
