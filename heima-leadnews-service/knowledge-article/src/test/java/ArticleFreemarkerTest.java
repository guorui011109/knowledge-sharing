import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleContent;
import com.knowledge.article.ArticleApplication;
import com.knowledge.article.mapper.ApArticleContentMapper;
import com.knowledge.article.mapper.ApArticleMapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.platform.commons.util.StringUtils;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest(classes = ArticleApplication.class)
@RunWith(SpringRunner.class)
public class ArticleFreemarkerTest {
    @Resource
    private ApArticleContentMapper apArticleContentMapper;
    @Resource
    private Configuration configuration;
    @Resource
    private FileStorageService fileStorageService;
    @Resource
    private ApArticleMapper apArticleMapper;
    @Test
    public void createStaticURL() throws IOException, TemplateException {
        //1.获取文章内容
        ApArticleContent apArticleContent = apArticleContentMapper.selectOne(new QueryWrapper<ApArticleContent>()
                .eq("id", 1302862388957036545L));
        if (apArticleContent != null && StringUtils.isNotBlank(apArticleContent.getContent())) {
            //2.通过Freemarker将文章内容生成为html静态文件
            Template template = configuration.getTemplate("article.ftl");
            Map<String,Object> content = new HashMap<>();
            content.put("content", JSONArray.parseArray(apArticleContent.getContent()));
            StringWriter out = new StringWriter();
            template.process(content,out);
            //3.将html文件上传到minio钟并得到对应路径
            InputStream in = new ByteArrayInputStream(out.toString().getBytes());
            String path = fileStorageService.uploadHtmlFile("", apArticleContent.getArticleId() + ".html", in);
            //4.将静态文件路径保存到apArticle表static_url字段中用于后续访问
            ApArticle apArticle = apArticleMapper.selectOne(new QueryWrapper<ApArticle>()
                    .eq("id", apArticleContent.getArticleId()));
            apArticle.setStaticUrl(path);
            apArticleMapper.updateById(apArticle);
        }
    }
}
