package com.knowledge.article.controller.v1;

import com.heima.common.constant.ArticleConstants;
import com.heima.model.article.dtos.ArticleHomeDTO;
import com.heima.model.common.dtos.ResponseResult;
import com.knowledge.article.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
@Api(value = "app端文章管理",tags = "app端文章管理")
@RestController
@RequestMapping("api/v1/article")
public class ArticleHomeController {
    @Resource
    private ArticleService articleService;

    /**
     * 加载首页
     * @param articleHomeDTO
     * @return
     */
    @ApiOperation("获取文章")
    @PostMapping("/load")
    public ResponseResult load(@RequestBody ArticleHomeDTO articleHomeDTO){
        return ResponseResult.okResult(articleService.load(articleHomeDTO, ArticleConstants.LOADTYPE_LOAD_NEW));
    }
    /**
     * 加载更多
     * @param articleHomeDTO
     * @return
     */
    @ApiOperation("获取更多文章")
    @PostMapping("/loadmore")
    public ResponseResult loadMore(@RequestBody ArticleHomeDTO articleHomeDTO){
        return ResponseResult.okResult(articleService.load(articleHomeDTO, ArticleConstants.LOADTYPE_LOAD_MORE));
    }
    /**
     * 加载最新
     * @param articleHomeDTO
     * @return
     */
    @ApiOperation("获取最新文章")
    @PostMapping("/loadnew")
    public ResponseResult loadNew(@RequestBody ArticleHomeDTO articleHomeDTO){
        return ResponseResult.okResult(articleService.load(articleHomeDTO, ArticleConstants.LOADTYPE_LOAD_NEW));
    }
}
