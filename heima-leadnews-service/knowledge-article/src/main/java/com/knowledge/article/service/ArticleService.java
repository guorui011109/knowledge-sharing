package com.knowledge.article.service;

import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDTO;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

import java.util.List;

public interface ArticleService {
    List<ApArticle> load(ArticleHomeDTO articleHomeDTO,Short type);

    /**
     * 保存app端相关文章
     * @param articleDto
     * @return
     */
    ResponseResult<String> saveArticle(ArticleDto articleDto);
}
