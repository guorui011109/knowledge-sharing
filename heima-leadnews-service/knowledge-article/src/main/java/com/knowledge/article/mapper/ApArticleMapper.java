package com.knowledge.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDTO;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {
    /**
     * 加载文章列表
     * @param articleHomeDTO 文章数据传输类
     * @param type 1加载更多 ， 2加载最新
     * @return List<ApArticle>
     */
     List<ApArticle> getArticleList(ArticleHomeDTO articleHomeDTO,Short type);
}
