package com.knowledge.article.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.pojos.ApArticle;
import com.knowledge.article.mapper.ApArticleMapper;
import com.knowledge.article.service.ArticleFreemarkerService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.HashMap;

@Service
@Transactional
public class ArticleFreemarkerServiceImpl implements ArticleFreemarkerService {
    @Resource
    private ApArticleMapper apArticleMapper;
    @Resource
    private Configuration configuration;
    @Resource
    private FileStorageService fileStorageService;
    @Override
    @Async
    public void buildArticleToMinIO(ApArticle apArticle, String content) {
        if (StringUtils.isNotBlank(content)){
            Template template;
            StringWriter out = new StringWriter();
            try {
                template = configuration.getTemplate("article.ftl");
                HashMap<String, Object> contentModel = new HashMap<>();
                contentModel.put("content", JSONArray.parseArray(content));
                template.process(contentModel,out);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            ByteArrayInputStream in = new ByteArrayInputStream(out.toString().getBytes());
            String path = fileStorageService.uploadHtmlFile("", apArticle.getId() + ".html", in);
            apArticle.setStaticUrl(path);
            apArticleMapper.updateById(apArticle);
        }
    }
}
