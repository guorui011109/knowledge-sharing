package com.knowledge.article.feign;

import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.knowledge.apis.article.IArticleClient;
import com.knowledge.article.service.ArticleService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ArticleClient implements IArticleClient {
    @Resource
    private ArticleService articleService;
    @PostMapping("/api/v1/article/save")
    @Override
    public ResponseResult<String> saveArticle(ArticleDto articleDto) {
        return articleService.saveArticle(articleDto);
    }
}
