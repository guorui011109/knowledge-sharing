package com.knowledge.article.service;

import com.heima.model.article.pojos.ApArticle;

public interface ArticleFreemarkerService {
    /**
     * 生成静态文件并上传至MinIO
     * @param apArticle
     * @param content
     */
    void buildArticleToMinIO(ApArticle apArticle,String content);
}
