package com.knowledge.article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.heima.common.constant.ArticleConstants;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDTO;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.knowledge.article.mapper.ApArticleConfigMapper;
import com.knowledge.article.mapper.ApArticleContentMapper;
import com.knowledge.article.mapper.ApArticleMapper;
import com.knowledge.article.service.ArticleFreemarkerService;
import com.knowledge.article.service.ArticleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class ArticleServiceImpl implements ArticleService {
    private final static Integer MAX_PAGE_SIZE = 50;
    @Resource
    private ApArticleMapper apArticleMapper;
    @Resource
    private ApArticleConfigMapper apArticleConfigMapper;
    @Resource
    private ApArticleContentMapper apArticleContentMapper;
    @Resource
    private ArticleFreemarkerService articleFreemarkerService;
    @Override
    public List<ApArticle> load(ArticleHomeDTO articleHomeDTO,Short type) {
        //1.分页条数校验
        Integer size = articleHomeDTO.getSize();
        if (size  == null || size == 0){
            size = 10;
        }
        size = Math.min(size, MAX_PAGE_SIZE);
        articleHomeDTO.setSize(size);
        //2.参数校验
        if (!type.equals(ArticleConstants.LOADTYPE_LOAD_MORE) && !type.equals(ArticleConstants.LOADTYPE_LOAD_NEW)){
            type = ArticleConstants.LOADTYPE_LOAD_MORE;
        }
        //3.频道参数校验
        if (StringUtils.isBlank(articleHomeDTO.getTag())){
            articleHomeDTO.setTag(ArticleConstants.DEFAULT_TAG);
        }
        //4.时间参数校验
        if (articleHomeDTO.getMaxBehotTime() == null){
            articleHomeDTO.setMaxBehotTime(new Date());
        }
        if (articleHomeDTO.getMinBehotTime() == null){
            articleHomeDTO.setMinBehotTime(new Date());
        }
        return apArticleMapper.getArticleList(articleHomeDTO, type);
    }
    @Transactional
    @Override
    public ResponseResult<String> saveArticle(ArticleDto articleDto) {
        //1.检查参数
        if (articleDto == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        ApArticle apArticle = new ApArticle();
        BeanUtils.copyProperties(articleDto,apArticle);
        //2.是否存在id
        if (articleDto.getId() == null) {
            //2.1 不存在id，保存 文章表 文章配置表 文章内容表
            //保存文章
            apArticleMapper.insert(apArticle);
            //保存配置
            ApArticleConfig apArticleConfig = new ApArticleConfig(apArticle.getId());
            apArticleConfigMapper.insert(apArticleConfig);
            //保存文章内容
            ApArticleContent apArticleContent = new ApArticleContent();
            apArticleContent.setArticleId(apArticle.getId());
            apArticleContent.setContent(articleDto.getContent());
            apArticleContentMapper.insert(apArticleContent);
        } else {
            //2.2 存在id 修改文章表 文章内容表
            apArticleMapper.updateById(apArticle);
            ApArticleContent apArticleContent = apArticleContentMapper.selectOne(new QueryWrapper<ApArticleContent>()
                    .eq("article_id", articleDto.getId()));
            apArticleContent.setContent(articleDto.getContent());
            apArticleContentMapper.updateById(apArticleContent);
        }
        //异步调用生成静态文件并上传至MinIO
        articleFreemarkerService.buildArticleToMinIO(apArticle,articleDto.getContent());
        //3. 结果返回文章id
        return ResponseResult.okResult(apArticle.getId());
    }
}
