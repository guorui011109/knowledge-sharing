package com.knowledge.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDTO;
import com.heima.model.user.pojos.ApUser;


public interface ApUserService extends IService<ApUser> {
    /**
     * app端登录功能
     * @param loginDTO
     * @return
     */
    ResponseResult login(LoginDTO loginDTO);
}
