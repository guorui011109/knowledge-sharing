package com.knowledge.user.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDTO;
import com.knowledge.user.service.ApUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
@Api(value = "app端用户登录",tags = "app端用户登录")
@RestController
@RequestMapping("/api/v1/login")
public class ApUserLoginController {
    @Resource
    private ApUserService apUserService;
    @PostMapping("/login_auth")
    @ApiOperation("登录接口")
    public ResponseResult login(@RequestBody LoginDTO loginDTO){
        return apUserService.login(loginDTO);
    }
}
