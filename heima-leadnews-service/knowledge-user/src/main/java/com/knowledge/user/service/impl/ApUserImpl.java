package com.knowledge.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.LoginDTO;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.common.AppJwtUtil;
import com.knowledge.user.mapper.ApUserMapper;
import com.knowledge.user.service.ApUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
@Slf4j
public class ApUserImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {
    @Resource
    private ApUserMapper apUserMapper;
    @Override
    public ResponseResult login(LoginDTO loginDTO) {
        Map<String,Object> map = new HashMap<>();
        //1.正常登录，需要用户名以及密码
        if (StringUtils.isNotBlank(loginDTO.getPhone()) && StringUtils.isNotBlank(loginDTO.getPassword())) {
            //1.1 根据用户名查询用户信息
            ApUser apUser = apUserMapper.selectOne(new QueryWrapper<ApUser>()
                    .eq("phone", loginDTO.getPhone()));
            if (apUser == null){
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"用户信息不存在");
            }
            //1.2 比对密码
            String salt = apUser.getSalt();
            String password = loginDTO.getPassword();
            String saltPassword = DigestUtils.md5DigestAsHex((password + salt).getBytes());
            if (!saltPassword.equals(apUser.getPassword())){
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }
            //1.3 返回数据 JWT
            String token = AppJwtUtil.getToken(apUser.getId().longValue());
            apUser.setSalt("");
            apUser.setPassword("");
            map.put("user",apUser);
            map.put("token",token);
        } else {
            //2.游客登录
            String token = AppJwtUtil.getToken(0L);
            map.put("token",token);
        }
        return ResponseResult.okResult(map);
    }
}
