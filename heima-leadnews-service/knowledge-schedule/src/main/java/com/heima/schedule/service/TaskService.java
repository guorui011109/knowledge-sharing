package com.heima.schedule.service;

import com.heima.model.schedule.dtos.Task;

public interface TaskService {
    /**
     * 添加延迟任务
     * @param task
     * @return
     */
     Long addTask(Task task);

    /**
     * 删除延迟任务
     * @param taskId
     * @return
     */
     boolean cancelTask(Long taskId);

    /**
     * 按照类型与优先级拉取任务
     * @param type 类型
     * @param priority 优先级
     * @return
     */
     Task pullTask(int type,int priority);
}
