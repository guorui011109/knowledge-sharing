package com.heima.schedule.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.heima.common.exception.CustomException;
import com.heima.common.redis.CacheService;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.schedule.dtos.Task;
import com.heima.model.schedule.pojos.Taskinfo;
import com.heima.model.schedule.pojos.TaskinfoLogs;
import com.heima.schedule.mapper.TaskinfoLogsMapper;
import com.heima.schedule.mapper.TaskinfoMapper;
import com.heima.schedule.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class TaskServiceImpl implements TaskService {
    @Resource
    private TaskinfoMapper taskinfoMapper;
    @Resource
    private TaskinfoLogsMapper taskinfoLogsMapper;
    @Resource
    private CacheService cacheService;
    /**
     * 添加延迟任务
     * @param task
     * @return
     */
    @Override
    public Long addTask(Task task) {
        //1.添加任务到数据库中
        Taskinfo taskinfo = new Taskinfo();
        BeanUtils.copyProperties(task,taskinfo);
        taskinfo.setTaskId(null);
        taskinfo.setExecuteTime(new Date(task.getExecuteTime()));
        taskinfoMapper.insert(taskinfo);
        TaskinfoLogs taskinfoLogs = new TaskinfoLogs();
        BeanUtils.copyProperties(taskinfo,taskinfoLogs);
        taskinfoLogs.setVersion(1);
        taskinfoLogs.setStatus(0);//初始化状态
        taskinfoLogsMapper.insert(taskinfoLogs);
        //2.添加任务到redis
        BeanUtils.copyProperties(taskinfo,task);
        task.setExecuteTime(taskinfo.getExecuteTime().getTime());
        addTaskToCache(task);
        return taskinfo.getTaskId();
    }

    @Override
    public boolean cancelTask(Long taskId) {
        boolean flag = false;
        //1.删除任务并更新任务日志（用于消费或取消任务均可）
        Task task = updateTask(taskId, 2);
        //2.删除redis缓存中的任务
        if (task != null){
            removeTaskCache(task);
            flag = true;
        }
        return flag;
    }

    @Override
    public Task pullTask(int type, int priority) {
        Task task = new Task();
        try {
            String key = "topic_" + type + "_" + priority;
            //从redis中拉取数据
            String taskJson = cacheService.lRightPop(key);
            //修改数据库信息
            if (StringUtils.isNotBlank(taskJson)) {
                task = JSON.parseObject(taskJson, Task.class);
                updateTask(task.getTaskId(), 1);
            }
        } catch (Exception e){
            log.error("拉取任务异常",e);
        }
        return task;
    }

    /**
     * 未来数据定时从set刷新到list中
     */
    @Scheduled(cron = "0 */1 * * * ?")
    public void refresh(){
        log.info("未来数据定时刷新{}", LocalDateTime.now());
        //获取所有set（未来数据）的集合key
        Set<String> futureKeys = cacheService.scan("future_*");
        //按照key和分支查询符合条件的数据
        for (String futureKey : futureKeys) {
            //获取当前数据的key
            String topicKey = "topic_"+ futureKey.split("future_")[1];
            Set<String> tasks = cacheService.zRangeByScore(futureKey, 0, System.currentTimeMillis());
            //同步数据
            if (!tasks.isEmpty()){
                cacheService.refreshWithPipeline(futureKey,topicKey,tasks);
            }
        }
        log.info("未来数据成功刷新{}", LocalDateTime.now());
    }

    /**
     * 数据库任务定时同步到redis
     */
    @Scheduled(cron = "0 */5 * * * ?")
    @PostConstruct
    public void reloadData(){
        //1.清理缓存中的数据
        clearCache();
        //2.查询符合条件的任务，小于未来五分钟的数据
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,5);//当前时间的五分钟之后
        List<Taskinfo> taskinfoList = taskinfoMapper.selectList(new QueryWrapper<Taskinfo>()
                .lt("execute_time", calendar.getTime()));
        //3.添加任务到redis
        if (taskinfoList != null && taskinfoList.size()>0){
            for (Taskinfo taskinfo : taskinfoList) {
                Task task = new Task();
                BeanUtils.copyProperties(taskinfo,task);
                task.setExecuteTime(taskinfo.getExecuteTime().getTime());
                addTaskToCache(task);
            }
        }
    }

    /**
     * 清理缓存中的数据
     */
    public void clearCache(){
        Set<String> topicKeys = cacheService.scan("topic_*");
        Set<String> futureKeys = cacheService.scan("future_*");
        cacheService.delete(topicKeys);
        cacheService.delete(futureKeys);
    }
    private void removeTaskCache(Task task) {
        String key = task.getTaskType() +"_"+ task.getPriority();
        if (task.getExecuteTime() < System.currentTimeMillis()){
            cacheService.lRemove("topic_" + key, 0, JSON.toJSONString(task));
            System.out.println(JSON.toJSONString(task));
        } else {
            cacheService.sRemove("future_" + key,JSON.toJSONString(task));
        }
    }

    /**
     * //删除任务并更新任务日志（用于消费或取消任务均可）
     * @param taskId
     * @param status
     */
    private Task updateTask(Long taskId, int status) {
        Task task = null;
        try {
            taskinfoMapper.deleteById(taskId);
            TaskinfoLogs taskinfoLogs = taskinfoLogsMapper.selectById(taskId);
            taskinfoLogs.setStatus(status);
            taskinfoLogsMapper.updateById(taskinfoLogs);
            task = new Task();
            BeanUtils.copyProperties(taskinfoLogs,task);
            task.setExecuteTime(taskinfoLogs.getExecuteTime().getTime());
        } catch (Exception e) {
            log.error("task cancel exception:{}",e.getMessage());
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }
        return task;
    }

    /**
     * 把任务添加到redis中
     * @param task
     */
    private void addTaskToCache(Task task) {
        String key = task.getTaskType() +"_"+ task.getPriority();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,5);//当前时间的五分钟之后
        //2.1.若任务时间小于当前时间，则存入list
        if (task.getExecuteTime() <= System.currentTimeMillis()){
            cacheService.lLeftPush("topic_" + key, JSON.toJSONString(task));
        } else if (task.getExecuteTime() <= calendar.getTimeInMillis()) {
            //2.2.若任务时间大于当前时间且小于等于预设时间5分钟，存入zSet中
            cacheService.zAdd("future_"+key, JSON.toJSONString(task),task.getExecuteTime());
        }
    }
}
