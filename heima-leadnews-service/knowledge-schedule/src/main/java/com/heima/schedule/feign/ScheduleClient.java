package com.heima.schedule.feign;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.Task;
import com.heima.schedule.service.TaskService;
import com.knowledge.apis.schedule.IScheduleClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class ScheduleClient implements IScheduleClient {
    @Resource
    private TaskService taskService;

    /**
     * 添加延迟任务
     * @param task
     * @return
     */
    @PostMapping("api/v1/task/add")
    public ResponseResult addTask(@RequestBody Task task){
        return ResponseResult.okResult(taskService.addTask(task));
    };

    /**
     * 删除延迟任务
     * @param taskId
     * @return
     */
    @GetMapping("api/v1/task/{taskId}")
    public ResponseResult cancelTask(@PathVariable Long taskId){
        return ResponseResult.okResult(taskService.cancelTask(taskId));
    };

    /**
     * 按照类型与优先级拉取任务
     * @param type 类型
     * @param priority 优先级
     * @return
     */
    @GetMapping("api/v1/task/{type}/{priority}")
    public ResponseResult pullTask(@PathVariable int type,@PathVariable int priority){
        return ResponseResult.okResult(taskService.pullTask(type,priority));
    };
}
