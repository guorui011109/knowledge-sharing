package com.heima.schedule.test;

import com.heima.common.redis.CacheService;
import com.heima.schedule.ScheduleApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Set;

@SpringBootTest(classes = ScheduleApplication.class)
@RunWith(SpringRunner.class)
public class RedisTest {
    @Resource
    private CacheService cacheService;
    @Test
    public void ListTest(){
        String list001 = cacheService.lRightPop("list_001");
        System.out.println(list001);
    }
    @Test
    public void ZSetTest(){
        Set<String> zset001 = cacheService.zRangeByScore("zset_001", 0, 8888);
        System.out.println(zset001);
    }
}
