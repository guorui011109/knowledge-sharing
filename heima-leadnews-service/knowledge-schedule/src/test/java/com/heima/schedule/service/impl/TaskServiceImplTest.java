package com.heima.schedule.service.impl;

import com.heima.model.schedule.dtos.Task;
import com.heima.schedule.ScheduleApplication;
import com.heima.schedule.service.TaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.xml.crypto.Data;

import java.util.Date;

import static org.junit.Assert.*;
@SpringBootTest(classes = ScheduleApplication.class)
@RunWith(SpringRunner.class)
public class TaskServiceImplTest {
    @Resource
    private TaskService taskService;
    @Test
    public void addTask() {
        Task task = new Task();
        task.setTaskType(666);
        task.setParameters("Redis is cool".getBytes());
        task.setExecuteTime(new Date().getTime()+600);
        task.setPriority(10);
        Long taskId = taskService.addTask(task);
        System.out.println(taskId);
    }
}